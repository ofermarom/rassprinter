﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace TerminalScr
{
    class CommObject
    {
        public enum ConnectOver { SerialPort, NetServer, NetClient };

        private NetClient spn;
        private SerialPort sp;
        private ConnectOver ConnectType = ConnectOver.SerialPort;
        public CommObject(ConnectOver type, object port)
        {
            ConnectType = type;
            switch (type)
            {
                case ConnectOver.SerialPort: sp = (SerialPort)port; break;
                case ConnectOver.NetClient: spn = (NetClient)port; break;
            }
        }
        public bool IsOpen { get { return ConnectType == ConnectOver.SerialPort ? sp.IsOpen : spn.IsOpen; } }
        public int BytesToRead { get { return ConnectType == ConnectOver.SerialPort ? sp.BytesToRead : spn.BytesToRead; } }
        public string ReadExisting() { return ConnectType == ConnectOver.SerialPort ? sp.ReadExisting() : spn.ReadExisting(); }
        public void Write(string value)
        {
            if (ConnectType == ConnectOver.SerialPort)
                sp.Write(value);
            else if (ConnectType == ConnectOver.NetClient)
                spn.Write(value);
        }
        public bool RtsEnable { get { return ConnectType == ConnectOver.SerialPort ? sp.RtsEnable : false; } set { if (ConnectType == ConnectOver.SerialPort) sp.RtsEnable = value; } }
        public bool DtrEnable { get { return ConnectType == ConnectOver.SerialPort ? sp.DtrEnable : false; } set { if (ConnectType == ConnectOver.SerialPort) sp.DtrEnable = value; } }
        public void Write(byte[] bytes, int offset, int len)
        {
            if (ConnectType == ConnectOver.SerialPort)
                sp.Write(bytes, offset, len);
            else if (ConnectType == ConnectOver.NetClient)
                spn.Write(bytes, offset, len);

        }
        public int BytesToWrite { get { return ConnectType == ConnectOver.SerialPort ? sp.BytesToWrite : spn.BytesToWrite; } }
        public int WriteBufferSize { get { return sp.WriteBufferSize; } set { sp.WriteBufferSize = value; } }
    }
    class PrintBuff
    {
        public byte[] passbuffer;// = new byte[40000];
        public int BufferSize = 0;
        public PrintBuff(int size)
        {
            BufferSize = size;
            passbuffer = new byte[size];

        }

    }


    class Steamprinter
    {

        private CommObject sp;
        private string cmd = "<F_FOAMSENSOR>2</F_FOAMSENSOR>?<CONFIG_MAIN_BOARD>1</CONFIG_MAIN_BOARD>?<SPEED_PROFILE>1</SPEED_PROFILE>?<STOPONDOOROPEN>0</STOPONDOOROPEN>?<Z_UP_EXTRA>150</Z_UP_EXTRA>?<CALCULATED_SP_VAL>623</CALCULATED_SP_VAL>?<SET_WARMUP_INTERVAL>1</SET_WARMUP_INTERVAL>?<SET_WARMING_FREQ>4</SET_WARMING_FREQ>?<SET_SPIT_FREQ>4</SET_SPIT_FREQ>?<PODTYPE>1</PODTYPE>?<HFIRE>19</HFIRE>?<PWFIRE>12</PWFIRE>?<VT>112</VT>?<SPIT>600</SPIT>?<QAPRINTER>0</QAPRINTER>?<ADJUSTX>120</ADJUSTX>?<SET_Y_ADVANCED>380</SET_Y_ADVANCED>";

        private string extera_cmd = "";// "<YSTEP>300</YSTEP>?";
        public bool WireLess = false;
        public enum PrintStats { Idle, Printing, PrintFail, PrintSucess };
        public string DebugString = "", PssFileBaseName = "";
        private string PassName = "";
        private string[] FileOrder = new string[6] { "1", "2", "3", "4", "5", "6" };
        public PrintStats PrintStat = PrintStats.Idle;
        private bool PrintFromBuffer = false;
        private PrintBuff[] PrintBuffers;
        private enum DataModeExitStat { Sucess, Fail, Error }
        public int PrintingAdvanced = 0;
        public string printFrequancy = "",FlowMsg="";
        private bool ListnerEnabled = false;
        private string PortListenData = "";
        private bool Runok = true;
        private string ReadXmlVal(string xml)
        {
            string sml = PortListenData.Substring(PortListenData.IndexOf(xml + ">"));
            sml = sml.Substring(sml.IndexOf(">") + 1);
            sml = sml.Substring(0, sml.IndexOf("</"));
            string tmp1 = PortListenData.Substring(0, PortListenData.IndexOf("<" + xml + ">"));
            string tmp2 = PortListenData.Substring(PortListenData.IndexOf("</" + xml + ">") + xml.Length + 3);
            PortListenData = tmp1 + tmp2;
            return sml;
        }

        public void Abort()
        {
            Runok = false;
        }

        private void StartListen()
        {
            if (sp.BytesToRead > 0)
                sp.ReadExisting();
            PortListenData = "";
            ListnerEnabled = true;
        }
        string Rd = "";
        private void PrinterDataListner()
        {
            while (sp.IsOpen && Runok )
            {

                if (ListnerEnabled && sp.BytesToRead > 0)
                    Rd += sp.ReadExisting();
                if (Rd.IndexOf("\r") > -1)
                {
                    DebugString = Rd;
                    PortListenData += Rd;
                    FlowMsg = PortListenData;
                    Rd = "";
                }

                Thread.Sleep(10);
            }

            if (PortListenData.IndexOf("</XSTOP>") > -1)
                printFrequancy = ReadXmlVal("XSTOP");
        }

        public Steamprinter()
        {

        }


        private bool Cmdsend(string cmdex)
        {

            string ack = "/ACK>";
            if (sp.IsOpen)
            {
                StartListen();//  sp.ReadExisting();
                sp.Write(cmdex + ">?");
                int Timeout = 0;
                string st = "";
                while (Timeout < 2500 && Runok)
                {
                    //if ( sp.BytesToRead > 0)
                    //{
                    //  st += sp.ReadExisting();
                    if (PortListenData.IndexOf("</") > -1)//  st.IndexOf("</") > -1)
                        Console.WriteLine(PortListenData);//st.Trim());
                    if (PortListenData.IndexOf(ack) > -1)// st.IndexOf(ack) > -1)
                    {
                        //    DebugString = PortListenData;//    st.Trim();
                        // Console.WriteLine(st.Trim());
                        ListnerEnabled = false;
                        return true;
                    }
                    //}
                }
                Timeout++;
                Thread.Sleep(10);
            }
            ListnerEnabled = false;
            return false;
        }




        private bool SendAndWait(string cmdex, string prm, string ack)
        {
            string st = "";
            if (sp.IsOpen)
            {
                StartListen();// sp.ReadExisting();
                sp.Write("<" + cmdex + ">" + prm + "</" + cmdex + ">?");
                int Timeout = 0;

                while (Timeout < 1000 && Runok)
                {                    
                    if (PortListenData.IndexOf(ack) > -1)
                    {
                        ListnerEnabled = false;
                        return true;
                    }
                    Timeout++;
                    Thread.Sleep(10);
                }

            }
            Console.WriteLine("ACK FAIL " + st);
            ListnerEnabled = false;
            return false;
        }



        private bool WaitFor(string st, int interval)
        {
            int timeout = interval;
            string rec = "";
            //StartListen();
            ListnerEnabled = true;
            while (timeout-- > 0 && Runok)
            {                
                if (PortListenData.IndexOf(st) > -1)
                {
                    ListnerEnabled = false;
                    return true;
                }
                Thread.Sleep(1);
            }
            Console.WriteLine("Wait for fail =>" + st);
            ListnerEnabled = false;
            return false;
        }

        private string FilesPath = "";
        private bool DTR_RTS = false;

        private bool Ram_Select(bool Select)
        {

            DebugString = Select ? "RAM A SELECTED\r\n" : "RAM B SELECTED\r\n";
            if (DTR_RTS == false)
            {
                return SendAndWait("S_RAM_SELECT", Select ? "1" : "0", "ACK");
            }
            else
            {
                sp.RtsEnable = false;
                sp.DtrEnable = !Select;
                return true;
            }

        }

        private bool Ram_ChipSelect(bool Select)
        {
            if (DTR_RTS == false)
            {
                Console.WriteLine("CS-->" + Select.ToString());
                //    if (Select == false) sp.Write("++++++++++++");
                return SendAndWait("S_RAM_CS", Select ? "1" : "0", "ACK");

            }
            else
            {
                Console.WriteLine("RTS CS-->" + Select.ToString());
                sp.RtsEnable = Select;
                return true;
            }

        }
        private bool MotionMoveSweep(int PassNum)
        {
            if (!DTR_RTS)
            {

                SendAndWait("PRINT_WIN", "1", "ACK");
                //MotionMoveSpweep();

                SendAndWait("PRINT_WIN", "0", "ACK");
                //MotionReturnStartSweep();
                return true;

            }
            else
            {
                /*    if ((PassNum & 0x1) > 0)
                        return SendAndWait("RAMAF", "1", "ACK");
                    else
                        return SendAndWait("RAMBF", "1", "ACK");
                        */
                return true;
            }



        }

        private bool TryToExistDataMode()
        {
            StartListen();
            sp.Write("++++++++");
            if (WaitFor("</RAM_FU", 600))
                return true;
            return false;
        }

        public void Spit(SerialPort msp)
        {
            sp = new CommObject(CommObject.ConnectOver.SerialPort, msp);
            if (ListTrdStart)
            {
                Thread th = new Thread(new ThreadStart(PrinterDataListner));
                th.Start();
                ListTrdStart = false;
            }
            SendAndWait("NO_DTR_RTS", DTR_RTS ? "0" : "1", "ACK");

            Ram_Select(true);
            Thread.Sleep(3);
            Ram_Select(false);
            Thread.Sleep(3);
            Ram_ChipSelect(true);
            ExistDataMode();


            //PrintStat = PrintStats.Printing;
            string[] commands = (extera_cmd + cmd).Split('?');
            int startrimr = DateTime.Now.Millisecond;
            Ram_Select(true);
            foreach (string cm in commands)
            {
                if (cm.Length > 4)
                {
                    if (Cmdsend(cm) == false)
                    {
                        PrintStat = PrintStats.PrintFail;
                        break;
                    }
                }
                else
                    break;
            }
            Cmdsend("<PARASPIT>1</PARASPIT>?");

            //sp.Write("<PARASPIT></PARASPIT>?");

        }

        public void ResetPrinter()
        {
            sp.Write("<RESET></RESET>?");
        }


        private DataModeExitStat ExistDataMode()
        {
            if (TryToExistDataMode()) return DataModeExitStat.Sucess;
            Thread.Sleep(200);
            if (TryToExistDataMode()) return DataModeExitStat.Sucess;
            sp.Write("<TEST></TEST>?");
            Thread.Sleep(200);
            string st = sp.ReadExisting();
            if (st.IndexOf("TEST") > -1) return DataModeExitStat.Fail;
            Console.WriteLine("Data Mode Exit fail!!!!" + st);
            return DataModeExitStat.Error;
        }

        private bool firsttime = true;
        private bool SendPass(int PassNum)
        {
            byte[] RamOpenZeroAdd = new byte[4] { 0x02, 0x00, 0x00, 0x00 };
            if (PassNum > 0 && PassNum < 7)
            {
                string path = "";
                if (FilesPath.Length > 1)
                    path = FilesPath + "\\passFile_";//     "C:\\workspace\\Dropbox\\Steam\\Printer_inits_codes\\passFile_";
                else
                    path = PassName;// "passFile_";
                byte[] by;

               // if (PrintFromBuffer)
               // {
                //    path = "genpass_";
                 //   by = PrintBuffers[PassNum - 1].passbuffer;
               // }
               // else
                //{
                    by = File.ReadAllBytes(/*path +*/ FileOrder[PassNum - 1]);// PassNum.ToString());
                    Console.WriteLine("DO now " + FileOrder[PassNum - 1]);
                //}

                if (PassNum > 1)// && DTR_RTS == false)
                {
                    if (WaitFor("/PASS>", 1000) == false)
                    {
                        Console.WriteLine("Fail to get pass end signal ");
                        return false;
                    }
                }
                else if (SendAndWait("STPRN", "1", "ACK") == false) // first seep only 
                    return false;

                /*
                if(firsttime)
                {
                    Ram_Select(true);
                    sp.DtrEnable = true;

                    if (Ram_ChipSelect(true))
                    {                        
                        sp.Write(RamOpenZeroAdd, 0, 4);
                        while (sp.BytesToWrite > 0) ;
                        sp.Write(by, 0, by.Length);
                        while (sp.BytesToWrite > 0) ;
                        Thread.Sleep(20);
                        Ram_ChipSelect(false);
                        Thread.Sleep(120);

                    }

                 

                    firsttime = false;
                }*/

                if (PassNum == 1)
                {
                    Ram_Select((PassNum & 0x1) > 0);
                    sp.DtrEnable = true;
                }
                else
                  if (WaitFor("RAM_FREE", 7900) == false) return false;

                while (true && Runok)
                {
                    if (Ram_ChipSelect(true))
                    {
                        Console.WriteLine("Clean Pass--> " + PassName.ToString());
                        //  if (PrintFromBuffer)
                        sp.Write(RamOpenZeroAdd, 0, 4);
                        while (sp.BytesToWrite > 0) ;
                        //********* Send print Data 
                        int SendDelay = 20;// by.Length / 600;////  90;//90
                        sp.Write(by, 0, by.Length);
                        while (sp.BytesToWrite > 0) ;
                        if (WireLess)
                            Thread.Sleep(2500);//extra for BT 

                        //Thread.Sleep(SendDelay);
                        Console.WriteLine("Buffer size " + by.Length.ToString() + "  " + SendDelay.ToString());
                        Thread.Sleep(SendDelay);
                        //**************************


                        DataModeExitStat DataModeExit = DataModeExitStat.Error;
                        if (DTR_RTS)
                        {
                            Ram_ChipSelect(false);
                            DataModeExit = WaitFor("</RAM_FULL>", 500) ? DataModeExitStat.Sucess : DataModeExitStat.Fail;
                        }
                        else
                        {
                            DataModeExit = ExistDataMode();
                        }



                        if (DataModeExit == DataModeExitStat.Sucess)
                        {
                            if (DTR_RTS)
                            {
                                if ((PassNum & 0x1) > 0)
                                {
                                    sp.RtsEnable = false;
                                    sp.DtrEnable = false;
                                    SendAndWait("RAMAF", "1", "ACK");
                                }
                                else
                                {
                                    sp.RtsEnable = false;
                                    sp.DtrEnable = true;
                                    SendAndWait("RAMBF", "1", "ACK");
                                }
                                Console.WriteLine("ram pass" + PassNum.ToString());
                                return true;//  WaitFor("RAM_FREE", 7900);
                            }
                            else
                            {
                                Ram_ChipSelect(false);
                                if (Ram_Select((PassNum & 0x1) == 0)) // change ram
                                {
                                    if ((PassNum & 0x1) > 0)
                                        SendAndWait("RAMAF", "1", "ACK");
                                    else
                                        SendAndWait("RAMBF", "1", "ACK");

                                    return true;
                                }
                            }
                        }
                        else if (DataModeExit == DataModeExitStat.Error)
                            return false;
                        else
                            Console.WriteLine("Print Data Send fail retry !!!");




                    }
                    else
                    {
                        Console.WriteLine("ERROR No Data Ack");
                        ExistDataMode();// sp.Write("++++++"); // exist data mode
                    }

                }


            }

            SendAndWait("RESET", "0", "HOST");
            return false;
        }


        private bool ListTrdStart = true;
        private bool Print()
        {
            if (ListTrdStart)
            {
                Thread th = new Thread(new ThreadStart(PrinterDataListner));
                th.Start();
                ListTrdStart = false;
            }

            SendAndWait("NO_DTR_RTS", DTR_RTS ? "0" : "1", "ACK");
            PrintStat = PrintStats.Printing;
            string[] commands = (extera_cmd + cmd).Split('?');
            int startrimr = DateTime.Now.Millisecond;
            Ram_Select(true);
            foreach (string cm in commands)
            {
                if (cm.Length > 4)
                {
                    if (Cmdsend(cm) == false)
                    {
                        PrintStat = PrintStats.PrintFail;
                        break;
                    }
                }
                else
                    break;
            }
            Cmdsend("<LIFTZ>1</LIFTZ>?");
            PrintingAdvanced++;
            int count = 0;
            Console.WriteLine("Interval ==" + (DateTime.Now.Millisecond - startrimr).ToString());
            if (WaitFor("/SPIT_END>", 3000))
            {
                if (PssFileBaseName.IndexOf(",") > -1)
                {
                    string[] cmds = PssFileBaseName.Split(',');
                    PassName = cmds[0];
                    string[] ord = cmds[1].Split(' ');
                    int i = 0;


                    foreach (string r in cmds)//ord
                    {
                        if (r.Length > 3)
                            FileOrder[i++] = r;
                        Console.WriteLine("PX=>" + r);
                    }
                }
                else
                    PassName = PssFileBaseName;



                for (int i = 1; i < 7; i++)
                {
                    PrintingAdvanced++;
                    if (SendPass(i) == false)
                    {
                        ListnerEnabled = false;
                        PrintStat = PrintStats.PrintFail;
                        break;//send and print pass
                    }
                    else
                        MotionMoveSweep(i);
                }

            }

            SendAndWait("DOWNZ", "0", "ACK");

            Console.WriteLine("Total " + count.ToString());

            WaitFor("ENDPRN", 3000);
            ListnerEnabled = false;
            if (PrintStat != PrintStats.PrintFail) PrintStat = PrintStats.PrintSucess;
            ListnerEnabled = false;
            return PrintStat != PrintStats.PrintFail;
        }

        public void StartPrint(NetClient msp, string passfilenae, bool DtrRtsMode)
        {
            DTR_RTS = DtrRtsMode;
            sp = new CommObject(CommObject.ConnectOver.NetClient, msp);
            PssFileBaseName = passfilenae;
            //   SendAndWait("NO_DTR_RTS", DtrRtsMode ? "0" : "1", "ACK");
            Print();
        }


        public void StartPrint(NetClient msp, string passfilenae)
        {
            sp = new CommObject(CommObject.ConnectOver.NetClient, msp);
            PssFileBaseName = passfilenae;
            //SendAndWait("NO_DTR_RTS", DtrRtsMode ? "0" : "1", "ACK");
            Print();
        }


        public void StartPrint(SerialPort msp, string passfilenae)
        {

            PrintFromBuffer = false;
            sp = new CommObject(CommObject.ConnectOver.SerialPort, msp);
            PssFileBaseName = passfilenae;
            //SendAndWait("NO_DTR_RTS", DtrRtsMode ? "0" : "1", "ACK");
            Print();
            GC.Collect();
            Console.WriteLine(GC.WaitForFullGCComplete());
        }

        public void StartPrint(SerialPort msp, string passfilenae, bool DtrRtsMode, string Extra)
        {
            PrintingAdvanced = 0;
            extera_cmd = Extra;
            DTR_RTS = DtrRtsMode;
            PrintFromBuffer = false;
            sp = new CommObject(CommObject.ConnectOver.SerialPort, msp);
            PssFileBaseName = passfilenae;
            PrintStat = PrintStats.Printing;
            //SendAndWait("NO_DTR_RTS", DtrRtsMode ? "0" : "1", "ACK");         
            Print();
        }

        public void StartPrint(SerialPort msp, ref PrintBuff[] PrintBuffer, bool DtrRtsMode, string extra)
        {
            extera_cmd = extra;
            DTR_RTS = DtrRtsMode;
            PrintFromBuffer = true;
            sp = new CommObject(CommObject.ConnectOver.SerialPort, msp);
            PrintBuffers = PrintBuffer;
            SendAndWait("NO_DTR_RTS", DtrRtsMode ? "0" : "1", "ACK");
            Print();
        }


        public void StartPrint(SerialPort msp, ref PrintBuff[] PrintBuffer)
        {
            PrintFromBuffer = true;
            sp = new CommObject(CommObject.ConnectOver.SerialPort, msp);
            PrintBuffers = PrintBuffer;
            // SendAndWait("NO_DTR_RTS", DtrRtsMode ? "0" : "1", "ACK");
            Print();
        }



    }
}
