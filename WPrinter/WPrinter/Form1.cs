﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using TerminalScr;
namespace WPrinter
{
    public partial class Form1 : Form
    {
        SerialPort sp = new SerialPort("COM1",921600, Parity.None, 8, StopBits.One);
        udpinterface server = new udpinterface();
        udpinterface client = new udpinterface();

        bool LINUX_VER = true;
        private Steamprinter Steamprinter_= new Steamprinter();
        private string PassfileName="";
        private bool Running = true;
        public Form1()
        {
            InitializeComponent();
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports) listBox1.Items.Add(port);
            listBox1.DoubleClick += ListBox1_DoubleClick;
            timer1.Enabled = true;
            this.FormClosing += Form1_FormClosing;
            CheckForPassF();

            try
            {
                sp.PortName = "/dev/ttyUSB0";
                sp.Open();
                if (PassfileName.Length > 5) button1.Visible = true;
                listBox1.Visible = false;
                

            }
            catch
            {
                infotab.Text = "LINUX PORT FAil ";
            }

            server.Server("127.0.0.1", 8000);
        //    client.Client("127.0.0.1", 8001);
          //  this.MouseClick += Form1_MouseClick;
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
           client.Send("Press\r\n");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void CheckForPassF()
        {
            try
            {
                Console.WriteLine("Get Files "+ System.IO.Directory.GetCurrentDirectory());
               // if(LINUX_VER)
                    string[] files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + "/pass");
                //else
               //     string[] files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + "\\pass");
                Console.WriteLine("Get Files " + System.IO.Directory.GetCurrentDirectory());
                PassfileName = "";
                
                foreach (string file in files)
                {
                    Console.WriteLine("Get Files "+ file );
                    string f = file;
                    if (f.IndexOf(".bdt") > -1)
                    {
                        PassfileName += f + ",";//.Substring(0, f.IndexOf(".bdt"))+",";
                                                //button1.Visible = true;
                    }
                    //else
                    //  PassfileName += f + ",";
                }
            }
            catch { }

        }

        private bool LoadPassF(string pas)
        {
            try
            {
                Console.WriteLine("Get Files " + System.IO.Directory.GetCurrentDirectory());                
                string[] files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + "/"+pas);
                             
                PassfileName = "";
                int pfiles = 0;
                foreach (string file in files)
                {
                    Console.WriteLine("Get Files " + file);
                    string f = file;
                    if (f.IndexOf(".bdt") > -1)
                    {
                        PassfileName += f + ",";
                        pfiles++;
                    }
                    
                }
                if(pfiles > 4)
                    return true;
            }
            catch
            {
                CheckForPassF();
                return true;

            }
            return false;
        }



        private void ListBox1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                sp.PortName = listBox1.Items[listBox1.SelectedIndex].ToString();
                sp.Open();
                if (PassfileName.Length > 5) button1.Visible = true;
                listBox1.Visible = false;
                SkipBootloader();

            }
            catch
            {

            }

        }
        private double GetVal(string s)
        {
            try { return Convert.ToDouble(s); } catch { }
            return 0;
        }

        private string GetVals(string x)
        {
            return (GetVal(x) * 100).ToString();
        }


        private string Xml(string xml, string val)
        {

            return "<" + xml + ">" + val + "</" + xml + ">?";
        }

        private bool SendXml(string xml, string val)
        {
            try
            {
                if (sp.IsOpen)
                {
                    Console.WriteLine("Read port");
                    if(sp.BytesToRead >0)
                     sp.ReadExisting();
                    Console.WriteLine("Write ");
                    sp.Write("<" + xml + ">" + val + "</" + xml + ">?");
                    Console.WriteLine("<" + xml + ">" + val + "</" + xml + ">?");
                    int timeout = 100;
                    string rx = "";
                    Console.WriteLine("Wait ");
                    while (timeout-- > 0 && Running)
                    {
                        if (sp.BytesToRead > 0)
                        {
                            rx += sp.ReadExisting();
                            Console.WriteLine(rx);
                        }

                        if (rx.IndexOf("/ACK>") > -1)
                        {
                            Console.WriteLine(rx);
                            return true;
                        }
                        Thread.Sleep(10);
                    }

                }
            }catch
            {
                Console.WriteLine("Port Error");

            }

            return false;
        }


        private void SendAxisPrameters()
        {
            while (Running)
            {
                button1.BackColor = Color.Red;

                /*                if (SendXml("SET_PID_PRAM", GetVals(textBox1.Text) + " 0") == false) break;
                                if (SendXml("SET_PID_PRAM", GetVals(textBox2.Text) + " 1") == false) break;
                                if (SendXml("SET_PID_PRAM", GetVals(textBox3.Text) + " 2") == false) break;
                                if (SendXml("SET_PID_PRAM", GetVals(textBox4.Text) + " 3") == false) break;
                                if (SendXml("SET_PID_PRAM", GetVals(textBox5.Text) + " 4") == false) break;
                                if (SendXml("SET_PID_PRAM", GetVals(textBox6.Text) + " 5") == false) break;
                                if (SendXml("X_LOCK", Xlock.Checked ? "1" : "0") == false) break;

                                if (SendXml("Y_DEF_START_FREQ", GetYstepperFreq(Ystartfreq.Text)) == false) break;
                                if (SendXml("Y_DEF_FREQ", GetYstepperFreq(YSpeed.Text.ToString())) == false) break;
                                if (SendXml("SET_Y_ADVANCED", Ystep.Text) == false) break;
                                if (SendXml("X_PRINT_SPEED", XTargetSpeed.Text) == false) break;

                                if (SendXml("STOP_X_LEN", textBox7.Text) == false) break;
                                //   if (SendXml("Z_UP_EXTRA", Zextera.Text) == false) break;
                                //if (SendXml("SET_X_TAR_SPEED", textBox8.Text) == false) break;
                                break;*/
                button1.BackColor = Color.Green;
                break;
            }

        }



        private void StartPrint()
        {
            if (sp.IsOpen)
            {
                button1.Enabled = false;
                SendAxisPrameters();
                Thread trd = new Thread(new ThreadStart(Print));
                trd.Priority = ThreadPriority.Highest;
                trd.Start();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sp.IsOpen)
            {
                button1.Enabled = false;
                SendAxisPrameters();
                Thread trd = new Thread(new ThreadStart(Print));
                trd.Priority = ThreadPriority.Highest;
                trd.Start();

            }

        }
        private bool Printing = false;
        private void Print()
        {
            Printing = true;
            string exttra = "";
          //  if (TEMP1.Text.Length > 0) exttra += "<TEMP1>" + TEMP1.Text + "</TEMP1>?";
          //  if (Temp2.Text.Length > 0) exttra += "<TEMP2>" + Temp2.Text + "</TEMP2>?";
          //  if (Temp3.Text.Length > 0) exttra += "<TEMP3>" + Temp3.Text + "</TEMP3>?";
           // if (Extera.Text.Length > 4) exttra += Extera.Text + "?";
           // exttra += "<Z_UP_EXTRA>" + Zextera.Text + "</Z_UP_EXTRA>?";
           // exttra += Xml("Y_DEF_START_FREQ", GetYstepperFreq(Ystartfreq.Text));
           // exttra += Xml("Y_DEF_FREQ", GetYstepperFreq(YSpeed.Text.ToString()));
            //exttra += Xml("SET_Y_ADVANCED", Ystep.Text);


            

            //int stopl = ((100 - (int)YStop.Value) * adv) / 100;
            //Console.WriteLine("Stop val =" + stopl.ToString());
            //exttra += Xml("Y_STOP_PROFILE", stopl.ToString());

            //exttra += Xml("Y_MICROSTEP", microstep.ToString());

            exttra +=Singlepass.Checked ? "<FASTPRN>1</FASTPRN>?" : "<FASTPRN>0</FASTPRN>?";
            //  < FASTPRN > 0 </ FASTPRN >?
            //"<LIFTZ>1</LIFTZ>?"
             Steamprinter_ .WireLess = WireLess.Checked;


            //if (PrintBuffers == null)
                Steamprinter_.StartPrint(sp, PassfileName/* "passFile_"*/, WireLess.Checked, GetSlideCmds + exttra);
           // else
             //   Steamprinter_.StartPrint(sp, ref PrintBuffers, WireLess.Checked, GetSlideCmds + exttra);

        //    GC.Collect();
         //   Console.WriteLine(GC.WaitForFullGCApproach());
            // button1.Enabled = true;
            Printing = false;
        }
        private string MakeXmlTag(string tag, string Value)
        {
            return "<" + tag + ">" + Value + "</" + tag + ">?";
        }

        private string GetSlideCmds
        {
            get
            {

                return MakeXmlTag("X_BACK_POS", "180") + MakeXmlTag("X_MOVE_POS", "1600") + MakeXmlTag("WIN_START_POS", "263") + MakeXmlTag("WIN_END_POS", "1300");
            }
        }


        private void loadPrintImageToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Image File  (*.bdt)|*.bdt|All files (*.*)|*.*";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                string fn = fd.FileName, dir = "";
                while (fn.IndexOf("\\") > -1)
                {
                    dir += fn.Substring(0, fn.IndexOf("\\") + 1);
                    fn = fn.Substring(fn.IndexOf("\\") + 1);
                }
                string[] files = System.IO.Directory.GetFiles(dir);
                PassfileName = "";
                foreach (string file in files)
                {
                    string f = file;
                    if (f.IndexOf(".bdt") > -1)
                    {
                        PassfileName += f + ",";//.Substring(0, f.IndexOf(".bdt"))+",";
                        button1.Visible = true;
                    }
                    else
                        PassfileName += f + ",";
                }

                // if (fn.IndexOf(".") > -1)
                //   PassfileName = fn.Substring(0, fn.IndexOf("."));
                // else
                //   PassfileName = fn;
                //PrintBuffers = null;
            }
        }

        private bool PrintLock = true;
        private int tm = 0;
        private bool Firstime = true;
        private void timer1_Tick(object sender, EventArgs e)
        {

            if(Firstime)
            {
                SkipBootloader();

                Firstime = false;
            }

            button1.Enabled = !Printing;
            button1.BackColor = Printing ? Color.Orange : Color.Green;
            if(Steamprinter_.PrintingAdvanced < PGS.Maximum)
                 PGS.Value = Steamprinter_.PrintingAdvanced;
            try
            {
                if (tm > 10)
                {
                    client.Send("Print," + Steamprinter_.PrintingAdvanced.ToString()+",\r","127.0.0.1",8001);
                    tm = 0;
                }
                else tm++;
            }
            catch
            {

            }

            try
            {
                string s = Steamprinter_.FlowMsg.Substring(Steamprinter_.FlowMsg.IndexOf(">") + 1);
                s = s.Substring(s.IndexOf("<"));
                infotab.Text = s;
            }
            catch { }

            if(server.MSGfromHost.IndexOf("\r")>-1)
            {
                infotab.Text = server.MSGfromHost;
                if (!Printing)
                {
                    if (server.MSGfromHost.IndexOf("PRN1") > -1)
                    {
                        LoadPassF("PAS1");
                        infotab.Text = "Printing!!!";
                       StartPrint();
                        Printing = true;
                    }

                    if (server.MSGfromHost.IndexOf("PRN2") > -1)
                    {
                        if (LoadPassF("PAS2"))
                        {
                            infotab.Text = "Printing!!!";
                            StartPrint();
                            Printing = true;
                        }
                    }

                    if (server.MSGfromHost.IndexOf("PRN3") > -1)
                    {
                        if (LoadPassF("PAS3"))
                        {
                            infotab.Text = "Printing!!!";
                            StartPrint();
                            Printing = true;
                        }
                    }
                    if (server.MSGfromHost.IndexOf("PRN4") > -1)
                    {
                        if (LoadPassF("PAS4"))
                        {
                            LoadPassF("PAS4");
                            infotab.Text = "Printing!!!";
                            StartPrint();
                            Printing = true;
                        }
                    }

                    if (server.MSGfromHost.IndexOf("PRN5") > -1)
                    {
                        if (LoadPassF("PAS5"))
                        {
                            infotab.Text = "Printing!!!";
                            StartPrint();
                            Printing = true;
                        }
                    }
                    if (server.MSGfromHost.IndexOf("PRN6") > -1)
                    {
                        if (LoadPassF("PAS6"))
                        {
                            infotab.Text = "Printing!!!";
                            StartPrint();
                            Printing = true;
                        }
                    }
                    if (server.MSGfromHost.IndexOf("SINGLE") > -1)
                        Singlepass.Checked = true;
                    if (server.MSGfromHost.IndexOf("DOUBLE") > -1)
                        Singlepass.Checked = false;
                    if (server.MSGfromHost.IndexOf("RESET") > -1)
                    {
                        Steamprinter_.ResetPrinter();

                    }
                    if (server.MSGfromHost.IndexOf("SPIT") > -1)
                    {
                        Steamprinter_.Spit(sp);

                    }


                }

                server.MSGfromHost = "";
            }

            //infotab.Text = Steamprinter_.FlowMsg;
        }


        private void SkipBootloader()
        {
            //09 09 09 55 AA AA AA 09 09 09 00 09 09
            byte[] bootskeep = new byte[14] { 0x00, 0x09, 0x09, 0x09, 0x55, 0xAA, 0xAA, 0xAA, 0x09, 0x09, 0x09, 0x00, 0x09, 0x09 };//  0x55, 0xaa, 0xaa, 0xaa };

            if (sp.IsOpen)
            {
                
                for (int i = 0; i < 13; i++)
                {
                    sp.Write(bootskeep, i, 1);
                    System.Threading.Thread.Sleep(10);

                }

                Steamprinter_.Spit(sp);
                
                    
                //sp.Write("<VERSION></VERSION>?");
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void abortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Steamprinter_.Abort();
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SendXml("RESET", "1");
            }catch
            {

                Console.WriteLine("Error test");
            }
        }
    }
}
