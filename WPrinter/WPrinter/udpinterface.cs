﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace WPrinter
{
    class udpinterface
    {
        private Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        private const int bufSize = 8 * 1024;
        private State state = new State();
        private EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
        private AsyncCallback recv = null;

        public string MSGfromHost = "";

        public class State
        {
            public byte[] buffer = new byte[bufSize];
        }

        public void Server(string address, int port)
        {
            _socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
            _socket.Bind(new IPEndPoint(IPAddress.Parse(address), port));
            Receive();
        }

        public void Client(string address, int port)
        {
            _socket.Connect(IPAddress.Parse(address), port);
            Receive();
        }

        public void Send(string text)
        {
            byte[] data = Encoding.ASCII.GetBytes(text);
            try
            {
                _socket.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) =>
                {
                    State so = (State)ar.AsyncState;
                    int bytes = _socket.EndSend(ar);
                    Console.WriteLine("SEND: {0}, {1}", bytes, text);
                }, state);
            }catch(Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }

        private bool SokFree = true;
        public void Send(string text,string address, int port)
        {
            if (SokFree)
            {
                SokFree = false;

                byte[] data = Encoding.ASCII.GetBytes("Hello World");
                
                try
                {
                    using (var client = new UdpClient())
                    {
                        IPEndPoint ep = new IPEndPoint(IPAddress.Parse(address), port);
                        client.Connect(ep);
                        client.Send(data, data.Length);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                /*
                try
                {
                    if(!_socket.Connected)
                      _socket.Connect(IPAddress.Parse(address), port);
                int timeout = 0;
                while (!_socket.Connected && timeout++ < 1000) System.Threading.Thread.Sleep(10);

                byte[] data = Encoding.ASCII.GetBytes(text);
                
                    _socket.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) =>
                    {
                        State so = (State)ar.AsyncState;
                        int bytes = _socket.EndSend(ar);
                        Console.WriteLine("SEND: {0}, {1}", bytes, text);
                    }, state);
                  //  _socket.Close();
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }*/
                //  _socket.Close();
                SokFree = true;
            }
        }


        private void Receive()
        {
            try
            {
                _socket.BeginReceiveFrom(state.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv = (ar) =>
                {
                    State so = (State)ar.AsyncState;
                    int bytes = _socket.EndReceiveFrom(ar, ref epFrom);
                    _socket.BeginReceiveFrom(so.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv, so);
                    Console.WriteLine("RECV: {0}: {1}, {2}", epFrom.ToString(), bytes, Encoding.ASCII.GetString(so.buffer, 0, bytes));
                    MSGfromHost = Encoding.ASCII.GetString(so.buffer, 0, bytes);
                }, state);
            }
            catch
            {

            }
        }


    }
}
