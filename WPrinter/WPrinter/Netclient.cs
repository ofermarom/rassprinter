﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;


namespace TerminalScr
{



    class NetClient
    {
        public byte[] byteData = new byte[60];
        public Socket socket;
        public string recmsg = "";
        public bool DataReciev = false, Cancel = false;

        public delegate void ServerReceivData(string rxd);//  byte[] rxd, int len);
        public event ServerReceivData RecData;

        public bool IsOpen { get { return IsConnected; } }

        public int BytesToRead { get { return recmsg.Length; } }
        private int bytestowrite = 0;


        public string ReadExisting()
        {
            string tmp = recmsg;
            recmsg = "";
            return tmp;
        }
        public void Write(string data)
        {
            byte[] array = Encoding.ASCII.GetBytes(data);
            SendToClient(array);
        }
        const int PackLen = 512;
        public void Write(byte[] buff, int offset, int len)
        {
            int sent = 0;
            bytestowrite = len;
            int tlen = len;
            Cancel = false;
            while (sent < tlen)
            {
                if (len > PackLen)
                {
                    PackAckFlag = false;
                    bytestowrite = PackLen;
                    socket.BeginSend(buff, sent, PackLen, SocketFlags.None, new AsyncCallback(OnSend), socket);
                    Console.Write("@$");
                    while (PackAckFlag == false && Cancel == false)
                        Thread.Sleep(5);
                    sent += PackLen;
                    len -= PackLen;
                    Thread.Sleep(250);
                }
                else
                {
                    len = tlen - sent;
                    bytestowrite = len;
                    socket.BeginSend(buff, sent, len, SocketFlags.None, new AsyncCallback(OnSend), socket);
                    while (BytesToWrite > 0) Thread.Sleep(5);
                    sent += PackLen;
                }

                //SendToClient(buff);
                if (Cancel) break;
            }

        }

        public int BytesToWrite
        { get { return bytestowrite; } }



        public NetClient(string ip, int port)
        {
            try
            {
                IPAddress _ip;
                if (IPAddress.TryParse(ip, out _ip) == false)
                {
                    IPHostEntry hst = Dns.GetHostEntry(ip);
                    ip = hst.AddressList[0].ToString();
                }
                // ----------------------------------------------------------------               
                socket = new Socket(IPAddress.Parse(ip).AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    socket.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
                    socket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnReceive), socket);
                }
                catch
                {

                }

            }
            catch (System.IO.IOException error)
            {
                Console.WriteLine(error.Message);
                //    _connected = false;
                //  throw (error);
            }
        }


        public void Flush()
        {
            for (int i = 0; i < 1024; i++) byteData[i] = 0;
        }

        public bool IsConnected
        {
            get
            {
                return socket.Connected;
            }

        }

        public void SendToClient(byte[] message)
        {
            socket.BeginSend(message, 0, message.Length, SocketFlags.None, new AsyncCallback(OnSend), socket);
        }

        public void SendToClient(byte[] message, int len)
        {
            socket.BeginSend(message, 0, len, SocketFlags.None, new AsyncCallback(OnSend), socket);
        }


        public void SendToClient(string msg)
        {
            byte[] message = Encoding.ASCII.GetBytes(msg);
            try
            {

                socket.BeginSend(message, 0, message.Length, SocketFlags.None, new AsyncCallback(OnSend), socket);
            }
            catch
            {
            }
        }

        public void OnSend(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                bytestowrite = 0;
                client.EndSend(ar);
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }
        bool PackAckFlag = false;
        private void OnReceive(IAsyncResult iar)
        {
            try
            {

                DataReciev = true;
                recmsg = Encoding.ASCII.GetString(byteData);
                if (recmsg.IndexOf("@") > -1)
                {
                    PackAckFlag = true;
                    Console.Write("@$");
                }
                // SendToClient(byteData);
                // if (RecData != null && recmsg.Substring(0,1) != null)
                //   RecData(recmsg);
                //recmsg = "";
                for (int i = 0; i < byteData.Length; i++) byteData[i] = 0;
                socket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnReceive), socket);
            }
            catch (Exception exp)
            {
                //DisconectEvent(this);
                try
                {
                    socket.Disconnect(true);
                }
                catch { }
                Console.WriteLine(exp.Message);
            }

        }

    }
}
